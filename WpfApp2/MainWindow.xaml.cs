﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace WpfApp2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button clickedButton = sender as Button;
            Window newWindow = null;

            switch (clickedButton.Content)
            {
                case "Canvas Example":
                    newWindow = new CanvasWindow();
                    break;
            }

            if (newWindow != null)
            {
                clickedButton.IsEnabled = false;

                newWindow.Show();

                newWindow.Closed += (x, y) => clickedButton.IsEnabled = true;
            }
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Close();
        }
    }
}